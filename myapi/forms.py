from django import forms
from .models import studentInfo
from dobwidget import DateOfBirthWidget

#Form for adding student information  
class studentForm(forms.ModelForm):
    
    class Meta:
        model = studentInfo
        fields = '__all__'
        widgets = {
        'studentName' :forms.TextInput(attrs={'class':'form-control'}),
        'fathersName' :forms.TextInput(attrs={'class':'form-control'}),
        'email' :forms.EmailInput(attrs={'class':'form-control'}),
        'address' :forms.TextInput(attrs={'class':'form-control'}),
        'city' :forms.TextInput(attrs={'class':'form-control'}),
        'dob': DateOfBirthWidget(attrs={'class':'form-control'}),
        'state' :forms.TextInput(attrs={'class':'form-control'}),
        
        'phoneNumber' :forms.TextInput(attrs={'class':'form-control'}),
        'pin' :forms.TextInput(attrs={'class':'form-control'}),
        'marks' :forms.TextInput(attrs={'class':'form-control'}),
        }