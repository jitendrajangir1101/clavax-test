from django.shortcuts import render
from django.shortcuts import render, get_object_or_404, redirect
from .forms import studentForm
from .models import studentInfo
from django.contrib import messages
from django.http import HttpResponseRedirect
import datetime
from datetime import timedelta
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import json
from rest_framework import serializers

# Create your views here.
def dashboard(request):
    students = studentInfo.objects.all().order_by('id')
    st = list(students.values())
    student_json = json.dumps(st,indent=4, sort_keys=True, default=str)
    print(student_json)
    page = request.GET.get('page', 1)

    paginator = Paginator(students, 2)
    try:
        students = paginator.page(page)
    except PageNotAnInteger:
        students = paginator.page(1)
    except EmptyPage:
        students = paginator.page(paginator.num_pages)
    return render(request,'myapi/dashboard.html',{'students':students , 'student_json':student_json})

#Function for adding new student details
def studentAdd(request):
    form = studentForm()
    if request.method == 'POST':
        form = studentForm(request.POST or None)
        if form.is_valid():
            studentName = form.cleaned_data['studentName']
            fathersName = form.cleaned_data['fathersName']
            dob = form.cleaned_data['dob']
            address = form.cleaned_data['address']
            city = form.cleaned_data['city']
            state = form.cleaned_data['state']
            pin = form.cleaned_data['pin']
            phoneNumber = form.cleaned_data['phoneNumber']
            email=form.cleaned_data['email']
            classOpted=form.cleaned_data['classOpted']
            marks=form.cleaned_data['marks']
            if (datetime.date.today() - dob) >= datetime.timedelta(days=10*365):
            
                studentObject = studentInfo(studentName=studentName,fathersName=fathersName,dob=dob,address=address,city=city,state=state,pin=pin,phoneNumber=phoneNumber,email=email,classOpted=classOpted,marks=marks)
                studentObject.save()
                messages.success(request, "you are registered with us !!!!")
                return HttpResponseRedirect('/dashboard/')
            else:
                messages.success(request, "Age should be greater than 10 !!!!")
                return HttpResponseRedirect('/studentAdd/')
    return render(request , 'myapi/studentAdd.html',{'form':form})

#Function for updating student details
def studentUpdate(request ,id):
    obj = get_object_or_404(studentInfo, id=id)

    form = studentForm(request.POST or None, instance=obj)
    
    context = {'form': form}
    if form.is_valid():
        obj = form.save(commit=False)
        obj.save()
        messages.success(request, "You successfully updated the post")
        return redirect('/dashboard/')

    else:
        context = {'form':form , 'error': 'The form was not updated successfully'}
        return render(request, 'myapi/studentAdd.html', context)

