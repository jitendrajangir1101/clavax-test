from django.urls import path
from . import views

urlpatterns = [
    path('dashboard/',views.dashboard ,name='dashboard'),
    path('studentAdd/',views.studentAdd ,name='studentAdd'),
    path('studentUpdate/<id>',views.studentUpdate,name='studentUpdate'),
]