from django.db import models

# Create your models here.
class studentInfo(models.Model):
    studentName = models.CharField(max_length=100)
    fathersName =models.CharField(max_length=100)
    dob = models.DateField()
    address = models.CharField(max_length=100)
    city = models.CharField(max_length=100)
    state = models.CharField(max_length=100)
    pin = models.CharField(max_length=100)
    phoneNumber=models.CharField(max_length=10 ,unique=True)
    email = models.EmailField(unique=True)
    c=(("5", "5"),("6","6"),("7","7"),("8","8"),("9","9"),("10","10"))
    classOpted=models.CharField(max_length=2,choices=c)
    marks = models.CharField(max_length=100)
    dateEnrolled = models.DateTimeField(auto_now=True)